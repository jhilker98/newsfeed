.. Newsfeed documentation master file, created by
   sphinx-quickstart on Wed Dec 16 13:20:57 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Newsfeed's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Newsfeed is a WIP RSS reader written in Python. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
